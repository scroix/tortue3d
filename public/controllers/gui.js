/*
 * [ Overview ]
 * Utilises the dat.gui library endorsed by three.js (used in their examples) as a simple way of modifying environmental variables.
 */

import {
  inventory,
  turtles,
  worldScene as scene,
  removeCompanions,
  addCompanions,
  swimming,
  feed
} from "../index.js";

// https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
// To adjust the light source colours.
const hexToRgb = hex =>
  hex
    .replace(
      /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => "#" + r + r + g + g + b + b
    )
    .substring(1)
    .match(/.{2}/g)
    .map(x => parseInt(x, 16));

// Menu items and their respective parameters.
var controller = function() {
  this.axes = false;
  this.wireframe = false;
  this.lighting = true;
  this.lightColour = "#ffffff";
  this.swimming = false;
  this.companions = false;
  this.feed = function() {
    feed();
  };
};

var gui = new dat.GUI({ hideable: false });
export const controllers = gui.__controllers;

// Strangly, window.onload triggers twice when hosted externally.
// This is a good alternative.
window.addEventListener("load", function() {
  initGUI();
});

// The menu functions.
function initGUI() {
  var con = new controller();

  gui
    .add(con, "swimming")
    .onChange(function() {
      swimming.active = !swimming.active;
    })
    .name("swimming (s)");

  gui.add(con, "feed").name("feed (f)");

  gui
    .add(con, "companions")
    .onChange(function() {
      if (con.companions == false) {
        removeCompanions();
      } else {
        addCompanions();
        // Now we've added the companions,
        // we should ensure GUI parameters are consistently applied.
        updateAxes(con.axes);
        updateMaterial(con.lighting, con.wireframe);
        updateWireframe(con.wireframe);
      }
    })
    .name("companions (c)");

  gui
    .add(con, "axes")
    .onChange(function() {
      updateAxes(con.axes);
    })
    .name("axes (x)");

  gui
    .add(con, "wireframe")
    .onChange(function() {
      updateWireframe(con.wireframe);
    })
    .name("wireframe (m)");

  gui
    .add(con, "lighting")
    .onChange(function() {
      updateMaterial(con.lighting, con.wireframe);
    })
    .name("lighting (l)");

  gui
    .addColor(con, "lightColour")
    .onChange(function() {
      inventory.forEach(item => {
        if (item.type == "DirectionalLight") {
          let rgb = hexToRgb(con.lightColour);
          item.color.r = ((rgb[0] / 255) * 1).toFixed(1);
          item.color.g = ((rgb[1] / 255) * 1).toFixed(1);
          item.color.b = ((rgb[2] / 255) * 1).toFixed(1);
        }
      });
    })
    .name("lightColour");
}

// A pair of functions to recursively update the wireframe status of materials.
function updateWireframe(wireframeFlag) {
  inventory.forEach(item => {
    _recursivelyUpdateWireframe(item, wireframeFlag);
  });
}

function _recursivelyUpdateWireframe(item, wireframeFlag) {
  if (item.hasOwnProperty("material")) {
    if (item.material.hasOwnProperty("wireframe")) {
      item.material.wireframe = wireframeFlag;
    }
  }
  if (item.children.length > 0) {
    item.children.forEach(child => {
      _recursivelyUpdateWireframe(child, wireframeFlag);
    });
  }
}

// A pair of functions to recursively swap between lambert and basic materials for the turtles.
// These materials are stored in the turtle object, outside of the three.js model, for easy access.
function updateMaterial(lambertFlag, wireframeFlag) {
  turtles.forEach(turtle => {
    _recursivelyUpdateMaterial(turtle, lambertFlag, wireframeFlag);
  });
}

function _recursivelyUpdateMaterial(turtle, lambertFlag, wireframeFlag) {
  // Update with the previously stored material.
  lambertFlag == true
    ? (turtle.body.shell.material = turtle.textures.shell.lambert)
    : (turtle.body.shell.material = turtle.textures.shell.basic);
  // Ensure that wireframes are consistent.
  turtle.body.shell.material.wireframe = wireframeFlag;
}

// A pair of functions for recursively updating the axes visibility across the scene.
function updateAxes(newValue) {
  scene.children.forEach(item => {
    _recursivelyUpdateAxes(item, newValue);
  });
}

function _recursivelyUpdateAxes(item, newValue) {
  // Use naming tags to identify and update visibility
  if (item.name == "axes") {
    item.visible = newValue;
  }
  // Recursivly iterate through scene children.
  if (item.children.length > 0) {
    item.children.forEach(child => {
      _recursivelyUpdateAxes(child, newValue);
    });
  }
}
