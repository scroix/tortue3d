/*
 * [ Contains ]
 * The event listeners for the keyboard, which utilise the animator class, and the GUI module to modify the scene.
 */

import { controllers } from "./gui.js";
import { feed, mainTurtle } from "../index.js";
import { JointRotator } from "../utils/animator.js";

const ARROW_KEYS = ["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"];
const ENUM_VALID_KEYS = {
  h: "hip",
  k: "knee",
  a: "ankle",
  n: "neck",
  t: "tail"
};
const ENUM_INCREMENT_KEYS = {
  "+": "increasing",
  "-": "decreasing",
  ArrowUp: "increasing",
  ArrowDown: "decreasing",
  ArrowRight: "increasing",
  ArrowLeft: "decreasing"
};

var currentlySelectedKey = null;

/* The primary keyboard event listener, responsible joint interactivity, and GUI controls */
document.addEventListener("keypress", event => {
  switch (event.key) {
    case "h":
    case "k":
    case "a":
    case "n":
    case "t":
      select(event.key);
      break;
    case "-":
    case "+":
      rotateSelection(event.key);
      break;
    case "f":
      feed();
      break;
    case "c":
      toggleController("companions");
      break;
    case "l":
      toggleController("lighting");
      break;
    case "m":
      toggleController("wireframe");
      break;
    case "s":
      toggleController("swimming");
      break;
    case "x":
      toggleController("axes");
      break;
    default:
      select(null);
      return;
  }
});

/* An additional event listener to handle arrow keys, because plus and minus were somewhat inconvenient when quickly manipulate joints */
document.addEventListener("keydown", event => {
  if (ARROW_KEYS.includes(event.key)) {
    rotateSelection(event.key);
  } else {
  }
});

/* Store the currently selected key. We'll refer to this when attempting to rotate joints */
function select(key) {
  currentlySelectedKey = key;
  updateSelectedUI(key);
}

var primaryRotator = new JointRotator(mainTurtle);

/* Controls to manipulate a single "primary" turtle, consider him a sort of alpha turtle. */
function rotateSelection(key) {
  // Which joint element are we looking for?
  let joint = ENUM_VALID_KEYS[currentlySelectedKey];
  // Increase or decrease depending on key.
  if (
    (key == "+" || key == "ArrowUp" || key == "ArrowRight") &&
    joint != null
  ) {
    primaryRotator.rotate(joint);
  } else if (
    (key == "-" || key == "ArrowDown" || key == "ArrowLeft") &&
    joint != null
  ) {
    primaryRotator.rotate(joint, true);
  }
  updateSelectedUI(key);
}

/* A simple text alert to provide feedback on selecting and manipulating joints */
function updateSelectedUI(key) {
  let ele = document.getElementById("currentlySelectedIndicator");
  // The key press is h, k, a, n or t.
  if (key in ENUM_VALID_KEYS) {
    ele.innerHTML = "selected: " + ENUM_VALID_KEYS[key];
  }
  // The key press is + or - and something was previously selected.
  else if (key in ENUM_INCREMENT_KEYS && ele.innerHTML.length > 0) {
    ele.innerHTML =
      ENUM_INCREMENT_KEYS[key] + ": " + ENUM_VALID_KEYS[currentlySelectedKey];
  }
  // An invalid selection.
  else {
    ele.innerHTML = "";
  }
}

/* Ensure that keyboard commands propogate to the GUI module */
function toggleController(name) {
  let controller = getController(name);
  if (controller) {
    controller.setValue(!controller.__prev);
  }
}

/* Retrieve the GUI controller responsible for a named parameter, i.e. 'Swimming' */
function getController(name) {
  for (var i in controllers) {
    if (controllers[i].property == name) {
      return controllers[i];
    }
  }
}
