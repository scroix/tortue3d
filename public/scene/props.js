/*
 * [ Contains ]
 * The components to create a fish bowl.
 * - a half sphere
 * - a secondary wireframe sphere slightly larger
 * - a refraction material
 * - water
 * - food pellets
 */

import { degreeToRadian } from "../utils/formulas.js";

export const FISH_BOWL_RADIUS = 20; // 20 is a good size for 4 turtles.

/* The sphere uses a refraction mapping for the see-through effect into the skybox. */
export function createHalfSphere(textureCube) {
  var geometry = new THREE.SphereBufferGeometry(
    FISH_BOWL_RADIUS,
    16,
    16,
    Math.PI / 2,
    Math.PI * 2,
    Math.PI / 2,
    Math.PI
  );

  textureCube.mapping = THREE.CubeRefractionMapping;

  var material = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    wireframe: false,
    side: THREE.DoubleSide,
    envMap: textureCube
  });

  var object = new THREE.Mesh(geometry, material);
  object.name = "halfsphere";

  return object;
}

/* An additional wireframe version, which is slightly larger, for aethestic styling. */
export function createWireframeHalfSphere(textureCube) {
  var geometry = new THREE.SphereBufferGeometry(
    FISH_BOWL_RADIUS + 0.1,
    16,
    16,
    Math.PI / 2,
    Math.PI * 2,
    Math.PI / 2,
    Math.PI
  );

  textureCube.mapping = THREE.CubeRefractionMapping;

  var material = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    wireframe: true,
    side: THREE.FrontSide
  });

  var object = new THREE.Mesh(geometry, material);
  object.name = "halfsphere";

  return object;
}

/* A plain, transparent water surface across the half sphere. */
export function createWaterSurface() {
  var geometry = new THREE.CircleBufferGeometry(FISH_BOWL_RADIUS, 16);
  var material = new THREE.MeshBasicMaterial({
    color: 0x0077be,
    side: THREE.DoubleSide,
    transparent: true,
    opacity: 0.33
  });
  var circle = new THREE.Mesh(geometry, material);
  circle.rotation.x += degreeToRadian(90);
  return circle;
}

/* Combine all the elements. */
export function buildCelestialFishBowl(textureCube) {
  let fishbowl_elements = [];
  fishbowl_elements.push(createHalfSphere(textureCube));
  fishbowl_elements.push(createWireframeHalfSphere(textureCube));
  fishbowl_elements.push(createWaterSurface());

  return fishbowl_elements;
}

/* Food pellets. The lack of material provided causes a randomised colour to be provided for each pellet. */
export function createFoodPellet() {
  var geometry = new THREE.IcosahedronBufferGeometry(0.25, 1);
  var icoshaedron = new THREE.Mesh(geometry);
  icoshaedron.name = "pellet";
  return icoshaedron;
}
