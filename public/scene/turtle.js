/*
 * [ Contains ]
 * Several major classes which are responsible for constructing the turtle.
 * These are embedded within each other, and are turned into Three.js objects with the construct() method.
 * There's also seperate functions for applying a custom texture, and "joint spheres" to identicate joint locations for debugging.
 */

import {
  createAxes,
  createSquareBipyramid,
  createHexagonalBipyramid
} from "../utils/geometry.js";

import { degreeToRadian } from "../utils/formulas.js";

export const TURTLE_SKIN_MATERIAL_LAMBERT = new THREE.MeshLambertMaterial({
  color: 0x78aa21,
  wireframe: false,
  side: THREE.DoubleSide,
  name: "skin"
});

export const TURTLE_SHELL_MATERIAL_LAMBERT = new THREE.MeshLambertMaterial({
  color: 0x8b5b33,
  wireframe: false,
  side: THREE.DoubleSide,
  name: "shell"
});

export const TURTLE_SKIN_MATERIAL_BASIC = new THREE.MeshBasicMaterial({
  color: 0x78aa21,
  wireframe: false,
  side: THREE.DoubleSide,
  name: "skin"
});

export const TURTLE_SHELL_MATERIAL_BASIC = new THREE.MeshBasicMaterial({
  color: 0x8b5b33,
  wireframe: false,
  side: THREE.DoubleSide,
  name: "shell"
});

// These can be overridden on construction of a new turtle.
export const DEFAULT_TURTLE_DIMENSIONS = {
  body: { height: 1.0, diameter: 0.5 },
  limbs: { sizeX: 0.5, sizeY: 0.3, sizeZ: 0.3 }
};
export const hexagonalBipyramidEdges = 6;
export const limbToRotations = {
  head: 0,
  flLeg: 1,
  rlLeg: 2,
  tail: 3,
  rrLeg: 4,
  frLeg: 5
};

/* A few utility parameters, and the host object for all the components consisting of a turtle. */
export class Turtle {
  constructor(
    nickname,
    randomisedColorFlag = true,
    dimensions = DEFAULT_TURTLE_DIMENSIONS
  ) {
    this.dimensions = dimensions;
    this.root = new THREE.Object3D(); // The three.js components, including children
    this.root.name = "turtle";
    this.root.nickname = nickname;
    this.feeding = false;
    this.targetFood = null;
    this.textures = this._getTextures(randomisedColorFlag);
    this.body = new Body(dimensions, this.textures);
  }

  construct() {
    this.root.add(this.body.constructBody());
    return this.root;
  }

  _getTextures(randomisedColorFlag) {
    var textures = {
      skin: {
        basic: TURTLE_SKIN_MATERIAL_BASIC.clone(),
        lambert: TURTLE_SKIN_MATERIAL_LAMBERT.clone()
      },
      shell: {
        basic: TURTLE_SHELL_MATERIAL_BASIC.clone(),
        lambert: TURTLE_SHELL_MATERIAL_LAMBERT.clone()
      }
    };
    if (randomisedColorFlag == true) {
      // Random skin colour.
      var colorSkin = new THREE.Color(Math.random() * 0xffffff);
      // Random shell colour.
      var colorShell = new THREE.Color(Math.random() * 0xffffff);
      // Apply the colours.
      for (var index in textures) {
        /* Just apply to shells for now 
        if (index == "skin") {
          textures[index].basic.color = colorSkin;
          textures[index].lambert.color = colorSkin;
        } */
        if (index == "shell") {
          textures[index].basic.color = colorShell;
          textures[index].lambert.color = colorShell;
        }
      }
    }
    return textures;
  }
}

/* Hosts all the limbs and shell. In charge of orientating each part appropriately around the shell. */
class Body {
  constructor(dimensions, textures, axesSize = 2) {
    this._setDefaultLimbSize(dimensions);
    this.textures = textures;
    this.shell = this._createShell(
      dimensions.body.height,
      dimensions.body.diameter,
      axesSize
    );
    this.frLeg = this._attachLeg(limbToRotations.frLeg, "frLeg");
    this.flLeg = this._attachLeg(limbToRotations.flLeg, "flLeg");
    this.rlLeg = this._attachLeg(limbToRotations.rlLeg, "rlLeg");
    this.rrLeg = this._attachLeg(limbToRotations.rrLeg, "rrLeg");
    this.top = this._attachHead(limbToRotations.head);
    this.tail = this._attachTail(limbToRotations.tail);
  }

  _setDefaultLimbSize(dimensions) {
    this.limbX = dimensions.limbs.sizeX;
    this.limbY = dimensions.limbs.sizeY;
    this.limbZ = dimensions.limbs.sizeZ;
  }

  constructBody() {
    let bodyParts = [
      this.top.construct(),
      this.frLeg.construct(),
      this.rlLeg.construct(),
      this.rrLeg.construct(),
      this.flLeg.construct(),
      this.tail.construct()
    ];
    bodyParts.forEach(item => {
      this.shell.add(item);
    });
    return this.shell;
  }

  _createShell(height, diameter, axesSize) {
    let shell = createHexagonalBipyramid(
      height,
      diameter,
      this.textures.shell.lambert
    );
    shell.add(createAxes(axesSize));
    /* Record the distance from the edges between each vertex along the shell's circumradius.
    We'll use this value to attach the legs, head and tail soon. */
    this.distanceFromShellEdge = Math.max.apply(
      Math,
      shell.geometry.vertices.map(function(vertex) {
        return vertex.x;
      })
    );
    // Load and rotate our texture.
    let loader = new THREE.TextureLoader();
    loader.load("textures/shell-tex.png", function(texture) {
      shell.material.map = texture;
    });
    return shell;
  }

  _attachLeg(limbPosition, label) {
    let leg = new Leg(
      this.distanceFromShellEdge,
      this.limbX,
      this.limbY,
      this.limbZ,
      this.textures
    );
    leg.name = label;
    // Exude from the dead center to be attached on the edge of the shell.
    // The hip (a joint) and upperLeg (a mesh) are exuded seperately to offset the center for rotation.
    leg.hip.position.x += this.distanceFromShellEdge;
    leg.upperLeg.position.x += this.limbX;
    // Rotate to correct position depending on limb position.
    leg.leg.rotation.y =
      ((Math.PI * 2) / hexagonalBipyramidEdges) * limbPosition;
    return leg;
  }

  _attachHead(limbPosition) {
    let headObj = new Head(this.limbX, this.limbY, this.limbZ);
    headObj.neck.position.x += this.distanceFromShellEdge; // 0.866...
    headObj.head.position.x += this.limbX; // ... + 0.5 = 1.366. with an offset center.
    headObj.neck.rotation.y =
      ((Math.PI * 2) / hexagonalBipyramidEdges) * limbPosition;
    return headObj;
  }

  _attachTail(limbPosition) {
    let tailObj = new Tail(this.limbX, this.limbZ);
    tailObj.tailbone.position.x -= this.distanceFromShellEdge;
    return tailObj;
  }
}

/* Construct a leg object containing a nested series of joints and limbs.
The individual components of the leg, joints and limbs, are rotated to proper form as their constructed with no requirement for parent transformations. */
class Leg {
  constructor(
    distanceFromShellEdge,
    sizeX,
    sizeY,
    sizeZ,
    textures,
    sizeAxes = 0.5
  ) {
    this.distanceFromShellEdge = distanceFromShellEdge;
    this.textures = textures;
    this.leg = new THREE.Object3D();
    this.hip = this._createJoint(sizeAxes, sizeX, true);
    this.upperLeg = this._createLimb(sizeX, sizeY, sizeZ, sizeAxes, true);
    this.knee = this._createJoint(sizeAxes, sizeX);
    this.lowerLeg = this._createLimb(sizeX, sizeY, sizeZ, sizeAxes);
    this.ankle = this._createJoint(sizeAxes, sizeX);
    this.foot = this._createLimb(sizeX, sizeY, sizeZ, sizeAxes);
  }

  construct() {
    this.ankle.add(this.foot);
    this.lowerLeg.add(this.ankle);
    this.knee.add(this.lowerLeg);
    this.upperLeg.add(this.knee);
    this.hip.add(this.upperLeg);
    this.leg.add(this.hip);
    return this.leg;
  }

  /* Joints are key for animation. 
  They're positioned at radial edge of their shapes and displayed by a miniature joint sphere.
  The class BODY handles rotation of the hips, so there is a flag attached to prevent doubling up on transformations. */
  _createJoint(sizeAxes, shapeRadius, attachedToShellFlag = false) {
    let joint = new THREE.Object3D();
    if (!attachedToShellFlag) {
      joint.rotation.z -= degreeToRadian(45);
      joint.position.x += shapeRadius;
    }
    joint.add(createAxes(sizeAxes));
    joint.add(createJointSphere()); // for debug
    return joint;
  }

  /* Limbs are visible meshes, containing geometry.
  They're offset slightly from their joints to aid the turtle's animation.
  A flag is used to account for the upperLeg which is rotated when attached to the shell in the BODY class. */
  _createLimb(sizeX, sizeY, sizeZ, sizeAxes, attachedToShellFlag = false) {
    let limb = createSquareBipyramid(
      sizeX,
      sizeY,
      sizeZ,
      this.textures.skin.lambert
    );
    if (!attachedToShellFlag) {
      limb.position.x += sizeX;
      limb.add(createAxes(sizeAxes));
    }
    return limb;
  }
}

class Tail {
  constructor(sizeX, sizeY) {
    this.tailbone = this._createTailbone();
    this.tail = this._createTail(sizeX, sizeY);
  }

  construct() {
    this.tailbone.add(this.tail);
    return this.tailbone;
  }

  _createTailbone() {
    var tailbone = new THREE.Object3D();
    tailbone.add(createJointSphere());
    return tailbone;
  }

  _createTail(sizeX, sizeZ) {
    let geometry = new THREE.Geometry();
    geometry.vertices.push(new THREE.Vector3(0, 0, 0));
    geometry.vertices.push(new THREE.Vector3(0, 0, sizeZ));
    geometry.vertices.push(new THREE.Vector3(0, 0, -sizeZ));
    geometry.vertices.push(new THREE.Vector3(-sizeX, 0, 0));

    geometry.faces.push(new THREE.Face3(0, 1, 3));
    geometry.faces.push(new THREE.Face3(0, 2, 3));
    geometry.computeFaceNormals();

    let material = TURTLE_SKIN_MATERIAL_LAMBERT;
    material.side = THREE.DoubleSide;

    return new THREE.Mesh(geometry, material);
  }
}

class Head {
  constructor(sizeX, sizeY, sizeZ, sizeAxes = 0.5) {
    this.neck = this._createNeck();
    this.head = this._createHead(sizeX, sizeY, sizeZ, sizeAxes);
    // These magical eyes shall use magical numbers.
    this.leftEye = this._createEye(sizeX, 0.15, 0.12, -0.12);
    this.rightEye = this._createEye(sizeX, 0.15, 0.12, 0.12);
  }

  construct() {
    this.head.add(this.rightEye);
    this.head.add(this.leftEye);
    this.neck.add(this.head);
    return this.neck;
  }

  _createNeck() {
    var neck = new THREE.Object3D();
    neck.add(createJointSphere()); // debug
    return neck;
  }

  _createHead(sizeX, sizeY, sizeZ, sizeAxes) {
    let head = createSquareBipyramid(
      sizeX,
      sizeY,
      sizeZ,
      TURTLE_SKIN_MATERIAL_LAMBERT
    );
    head.add(createAxes(sizeAxes));
    return head;
  }

  _createEye(sizeX, positionX, positionY, positionZ) {
    let geometry = new THREE.SphereGeometry(sizeX * 0.1);
    let material = new THREE.MeshBasicMaterial({ color: 0xf0f0f0 });
    let sphere = new THREE.Mesh(geometry, material);
    sphere.position.x = positionX;
    sphere.position.y = positionY;
    sphere.position.z = positionZ;
    return sphere;
  }
}

function createJointSphere() {
  let geometry = new THREE.SphereGeometry(0.05);
  let material = TURTLE_SHELL_MATERIAL_LAMBERT;
  let sphere = new THREE.Mesh(geometry, material);
  return sphere;
}

export function applyCustomTexture(turtle) {
  // Turtle Texture
  var shellTexture = new THREE.TextureLoader().load(
    "../textures/shell-tex.png"
  );

  // These are coordinates pulled straight out of A2,
  // and rely on the default dimensions for the turtle to be used.
  var coordinates = [
    { x: 1.0, y: 0.5 },
    { x: 0.75, y: 0.933 },
    { x: 0.25, y: 0.933 },
    { x: 0, y: 0.5 },
    { x: 0.25, y: 0.067 },
    { x: 0.75, y: 0.067 },
    { x: 0.5, y: 0.5 },
    { x: 0.5, y: 0.5 }
  ];

  var uvs = [
    [
      new THREE.Vector2(coordinates[0].x, coordinates[0].y),
      new THREE.Vector2(coordinates[1].x, coordinates[1].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[0].x, coordinates[0].y),
      new THREE.Vector2(coordinates[1].x, coordinates[1].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[1].x, coordinates[1].y),
      new THREE.Vector2(coordinates[2].x, coordinates[2].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[2].x, coordinates[2].y),
      new THREE.Vector2(coordinates[3].x, coordinates[3].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[3].x, coordinates[3].y),
      new THREE.Vector2(coordinates[4].x, coordinates[4].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[4].x, coordinates[4].y),
      new THREE.Vector2(coordinates[5].x, coordinates[5].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[5].x, coordinates[5].y),
      new THREE.Vector2(coordinates[0].x, coordinates[0].y),
      new THREE.Vector2(coordinates[6].x, coordinates[6].y)
    ],
    [
      new THREE.Vector2(coordinates[0].x, coordinates[0].y),
      new THREE.Vector2(coordinates[1].x, coordinates[1].y),
      new THREE.Vector2(coordinates[7].x, coordinates[7].y)
    ],
    [
      new THREE.Vector2(coordinates[1].x, coordinates[1].y),
      new THREE.Vector2(coordinates[2].x, coordinates[2].y),
      new THREE.Vector2(coordinates[7].x, coordinates[7].y)
    ],
    [
      new THREE.Vector2(coordinates[2].x, coordinates[2].y),
      new THREE.Vector2(coordinates[3].x, coordinates[3].y),
      new THREE.Vector2(coordinates[7].x, coordinates[7].y)
    ],
    [
      new THREE.Vector2(coordinates[3].x, coordinates[3].y),
      new THREE.Vector2(coordinates[4].x, coordinates[4].y),
      new THREE.Vector2(coordinates[7].x, coordinates[7].y)
    ],
    [
      new THREE.Vector2(coordinates[4].x, coordinates[4].y),
      new THREE.Vector2(coordinates[5].x, coordinates[5].y),
      new THREE.Vector2(coordinates[7].x, coordinates[7].y)
    ],
    [
      new THREE.Vector2(coordinates[5].x, coordinates[5].y),
      new THREE.Vector2(coordinates[0].x, coordinates[0].y),
      new THREE.Vector2(coordinates[7].x, coordinates[7].y)
    ]
  ];

  for (var i = 0; i < uvs.length; i++) {
    turtle.body.shell.geometry.faceVertexUvs[0].push(uvs[i]);
  }

  turtle.body.shell.geometry.uvsNeedUpdate = true;
  turtle.textures.shell.lambert.map = shellTexture;
}
