/*
 * ---------------------------\
 * Assignment 2 - Turtle Park |
 * Julien de-Sainte-Croix     |
 * s3369242                   |
 * ---------------------------/
 *
 * [ Overview ]
 * A Three.js program which renders one to many turtles, based on geometry from assignment 1.
 * The turtle(s) are rendered in a pool, consisting of a half sphere, and contained within a cube skybox.
 * There are rendering and environmental controls available via both keyboard, and dat.gui an additional third-party endorsed by three.js.
 *
 * [ Controls ]
 * The camera can be moved with the mouse.
 * The limbs of the main turtle are selectable via the keyboard, once selected, can be rotated with the +/- keys, or arrow keys.
 * There are also numerous keyboard shortcuts, which are listed in the dat.gui interface in the top-right of the canvas.
 *
 * [ Structure ]
 * index.js - responsible for combining the turtles, environment, animator and controllers.
 * /utils/animator.js - manages swimming, manual joint rotation, and "eating".
 * /utils/formulas.js - useful mathematics formulas used throughout the application.
 * /utils/geometry.js - octahedron and dodecahedron generator, the two main geometries in this application.
 * /textures/ - contains the skybox and shell textures, which are applied here in the index.
 * /scene/props.js - container for the fish bowl and related meshes.
 * /scene/turtle.js - generator for turtles, combines geometry and materials, and provides a utility turtle object used extensively throughout the application.
 * /controllers/gui.js - hud manager
 * /controller/keyboard.js - keyboard event driven manager
 * /controller/TrackballControls.js - third party camera manager
 */

import { createAxes } from "./utils/geometry.js";
import { randomIntFromInterval, distance } from "./utils/formulas.js";
import {
  buildCelestialFishBowl,
  createFoodPellet,
  FISH_BOWL_RADIUS
} from "./scene/props.js";
import { Turtle, applyCustomTexture } from "./scene/turtle.js";
import { Animator } from "./utils/animator.js";

/* Initalise scene. This is available to other modules. */
export var worldScene = new THREE.Scene();

/* Initalise camera. */
var camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
camera.position.z = 5;
camera.position.y = 2;

/* Initialise renderer and attach to DOM. */
var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0x404040, 1);
document.body.appendChild(renderer.domElement);

/* Generate scene lighting and skybox. */

// A white directional light aimed at the center, sourced from infinity on the z axes.
function createDirectionalLight() {
  var light = new THREE.DirectionalLight(0xffffff);
  light.position.set(0, 1, 0);
  return light;
}

// A dull grey ambient light.
function createAmbientLight() {
  var light = new THREE.AmbientLight(0x404040);
  return light;
}

// A simple skybox.
var loader = new THREE.CubeTextureLoader();
loader.setPath("textures/skybox/");
var textureCube = loader.load([
  "px.jpg",
  "nx.jpg",
  "py.jpg",
  "ny.jpg",
  "pz.jpg",
  "nz.jpg"
]);
worldScene.background = textureCube;

/* Our main star, Terry the Turtle.
He gets special treatment, including a texture applied to his shell, and control of his individual joints. */
const mainTurtleName = "Terry";
export const mainTurtle = new Turtle(mainTurtleName, false); // A utility object with the three.js object, and additional parameters.
const mainTurtleConstructed = mainTurtle.construct(); // A three.js object version to go straight into the scene.
applyCustomTexture(mainTurtle);

/* A series of functions managment of the world scene */

// Add a pellet to the scene, this is called via external controllers.
var foodMonitor = [];
export function feed() {
  // Generate a food pellet.
  var food = createFoodPellet();
  // Position the food pellet randomly within the fish bowl.
  let border = FISH_BOWL_RADIUS / 1.5;
  let randX = randomIntFromInterval(-border, border);
  let randZ = randomIntFromInterval(-border, border);
  food.position.x = randX;
  food.position.z = randZ;
  foodMonitor.push(food); // add to monitor
  worldScene.add(food); // add to actual scene
}

// Swallow/remove an exisiting food pellet from the scene.
export function swallow(pellet) {
  for (let i = 0; i < foodMonitor.length; i++) {
    if (foodMonitor[i].uuid == pellet.uuid) {
      foodMonitor.splice(i, 1); // remove from monitor
      worldScene.remove(pellet); // remove from actual scene
    }
  }
}

// Remove all turtles minus Terry, the main turtle.
export function removeCompanions() {
  // Remove companions from the object array used for the animator.
  while (turtles.length > 1) {
    turtles.pop();
  }
  // Remove companions from the THREE.JS scene.
  for (var i = worldScene.children.length - 1; i >= 0; i--) {
    let obj = worldScene.children[i];
    if (obj.name == "turtle") {
      if (obj.nickname != mainTurtleName) {
        worldScene.remove(obj);
      }
    }
  }
}

// Add compansions for Terry, they're generated freshly every time this is called.
export function addCompanions() {
  let companions = [
    new Turtle("Thomas"),
    new Turtle("Lucy"),
    new Turtle("Jeremy")
  ];
  // Randomise their spawn locations.
  var occupiedLocations = [
    [mainTurtle.root.position.x, mainTurtle.root.position.z]
  ];
  companions.forEach(companion => {
    let spawn = _getSafeSpawnLocation(occupiedLocations);
    companion.root.position.x = spawn.x;
    companion.root.position.z = spawn.y;
    occupiedLocations.push([spawn.x, spawn.y]);
    // Rotate the turtles randomly.
    companion.root.rotation.y = Math.random() * Math.PI * 2;
    // Build the turtles.
    var t = companion.construct();
    // Create the three.js objects for be added to the scene.
    worldScene.add(t);
    // Add the object references to our own array for the animator to easily manipulate limbs.
    turtles.push(companion);
    // Add the turtles to the scene inventory.
    inventory.push(t);
  });
}

const SAFE_SPAWN_DISTANCE = FISH_BOWL_RADIUS / 2;

function _getSafeSpawnLocation(exisitingSpawns) {
  let border = FISH_BOWL_RADIUS / 2;
  let safe = false;
  var proposed = { x: 0, y: 0 };
  // Check the updating list of used spawn locations.
  // There's an arbitrary barrier around them to prevent overlapping spawn locations.
  while (safe == false) {
    // Assume innocence.
    safe = true;
    // Generate a random vector.
    proposed.x = randomIntFromInterval(-border, border);
    proposed.y = randomIntFromInterval(-border, border);
    // Compare vector to exisiting spawn vectors.
    exisitingSpawns.forEach(spawn => {
      let distanceToSpawn = distance({ x: spawn[0], y: spawn[1] }, proposed);
      // If we're close to exisiting spawns, we're no longer safe/innoncent.
      if (distanceToSpawn < SAFE_SPAWN_DISTANCE) {
        safe = false;
      }
    });
  }
  return proposed;
}

// Combine the fish bowl, lights, axes and our star Terry to the scene.
function initaliseScene() {
  // Add our celestial fish bowl to the scene.
  let celestialFishBowl = buildCelestialFishBowl(textureCube);
  celestialFishBowl.forEach(element => {
    worldScene.add(element);
  });
  // Add our miscellaneous inventory to the scene.
  inventory.forEach(item => {
    worldScene.add(item);
  });
  // Add our main star, Terry the Turtle.
  worldScene.add(mainTurtleConstructed);
  // Rotate our star randomly.
  mainTurtle.root.rotation.y = Math.random() * Math.PI * 2;
  // Add our main turtle to the inventory for GUI management.
  inventory.push(mainTurtleConstructed);
  // And begin to render.
  renderer.render(worldScene, camera);
}

export var turtles = [mainTurtle]; // This may grow or shrink as companions are brought into the world.
export var inventory = [
  createAxes(10),
  createDirectionalLight(),
  createAmbientLight()
];

// The animator applies to all exisiting turtles.
var animator = new Animator(turtles, foodMonitor);
var swimmingManager = function() {
  this.active = false; // The use of a function here allows pass by reference for a boolean.
};

export var swimming = new swimmingManager();

/* Camera controller and animator looping on callbacks. */
var controls = new THREE.TrackballControls(camera, renderer.domElement);
controls.addEventListener("change", render);
animate();

function render() {
  renderer.render(worldScene, camera);
}

function animate() {
  render();
  requestAnimationFrame(animate);
  controls.update();
  if (swimming.active == true) {
    animator.update();
  }
}

/* Go! */
initaliseScene();
