/*
 * [ Overview ]
 * A series of useful formulas used throughout the application.
 */

export function degreeToRadian(degree) {
  return degree * (Math.PI / 180);
}

export function radiansToDegrees(radians) {
  var pi = Math.PI;
  return radians * (180 / pi);
}

/* Calculates the distance between two vectors using Pythagoras's Theorem. */
export function distance(p1, p2) {
  return Math.hypot(p2.x - p1.x, p2.y - p1.y);
}

/* Calculates the distance between two 3D vertices using Pythagoras's Theorem. */
export function distance3D(p1, p2) {
  var x = p1.x - p2.x;
  var y = p1.y - p2.y;
  var z = p1.z - p2.z;
  return Math.sqrt(x * x + y * y + z * z);
}

/* Generate a random interval between two numbers. 
@Francisc https://stackoverflow.com/questions/4959975/generate-random-number-between-two-numbers-in-javascript */
export function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
