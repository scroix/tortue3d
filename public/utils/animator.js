/*
 * [ Contains ]
 * A linear interpolation engine.
 * An animator class for an array of turtles.
 * A rotation class specifically for joints.
 */

import {
  degreeToRadian,
  distance3D,
  randomIntFromInterval
} from "./formulas.js";

import { FISH_BOWL_RADIUS } from "../scene/props.js";
import { swallow } from "../index.js";

function lerp(k1, v1, k2, v2, k) {
  return v1 + ((k - k1) / (k2 - k1)) * (v2 - v1);
}

function findInterval(keys, key) {
  /* takes an array of keys, and a value being interpolated.
      returns the index of the first key in the array
      which is greater than the value being interpolated. */
  for (var i = 0; i < keys.length; i++) {
    if (keys[i] > key) {
      return i;
    }
  }
}

function interpolator(keys, values, key) {
  /* Takes an array of keys, array of values, and a given key.
      Returns the interpolated value for the given key.
      Uses findInterval() and lerp() */
  let interval = findInterval(keys, key);
  let interpolatedValue = lerp(
    keys[interval - 1], // lower bound index of value being interpolated
    values[interval - 1],
    keys[interval], // upper bound index of value being interpolated
    values[interval],
    key
  );
  return interpolatedValue;
}

var clock = new THREE.Clock();
clock.start();
var t = 0;

/* Used to "move" all turtles, detect collisions, and "eat" pellets. */
export class Animator {
  constructor(turtles, food) {
    this.turtles = turtles;
    this.food = food;
    this.clock = new THREE.Clock();
    this.timeKeys = [0, 0.5, 1.0, 1.5, 2.0]; // One second loop, broken into five key frames.
  }

  update() {
    let dt = this.clock.getDelta();
    if (t >= this.timeKeys[this.timeKeys.length - 1] - dt) {
      t = 0;
    }
    t += dt;
    this.turtles.forEach(turtle => {
      // Animate each of the limbs.
      if (turtle.feeding == false) {
        this._rotateRightLegs([turtle.body.frLeg, turtle.body.rrLeg]);
        this._rotateLeftLegs([turtle.body.flLeg, turtle.body.rlLeg]);
        this._nod(turtle.body.top);
        this._wag(turtle.body.tail);
        // And move forward, rotating occasionally or on collision.
        this._move(turtle, dt);
        this._handleCollisions(turtle);
      }
      // Carefully turn to face the food, then eat and resume as normal.
      else {
        // Nudge towards food until we're facing it.
        if (this._rotateTowardsFood(turtle) == true) {
          // Eat the food until finished, and feeding flag is toggled.
          this._eatFood(turtle);
        }
      }
    });
  }

  _handleCollisions(turtle) {
    let collision = false;
    let threshold = 3.5;
    let turtleBoundingSphereSize =
      turtle.body.shell.geometry.boundingSphere.radius +
      turtle.dimensions.limbs.sizeX * threshold;
    let turtlePosition = turtle.root.position;

    // Rotate when you crash into the fish bowl.
    let distanceToFishBowl = distance3D(turtle.root.position, {
      x: 0,
      y: 0,
      z: 0
    });
    if (distanceToFishBowl + turtleBoundingSphereSize > FISH_BOWL_RADIUS) {
      collision = true;
    }

    // Rotate when you crash into another turtle.
    for (let t of this.turtles) {
      if (t.root.nickname != turtle.root.nickname) {
        let distanceToTurtle = distance3D(
          turtle.root.position,
          t.root.position
        );
        if (distanceToTurtle < turtleBoundingSphereSize) {
          collision = true;
          break;
        }
      }
    }

    // Collision with food toggles turtle status.
    for (let f of this.food) {
      let distanceToFood = distance3D(turtle.root.position, f.position);
      if (distanceToFood < turtleBoundingSphereSize) {
        turtle.feeding = true;
        turtle.targetFood = f;
        return;
      }
    }

    // React to a collision.
    if (collision) {
      this._move(turtle, true);
      this._rotate(turtle, true); // Spin backwards.
    }
  }

  _rotate(turtle, reverse = false) {
    let direction = 0;
    let reversalRange = 20;
    reverse == true // 180 += variable degrees on reversal, 0 to 360 degree rotation otherwise.
      ? (direction = randomIntFromInterval(
          180 - reversalRange,
          180 + reversalRange
        ))
      : (direction = Math.random() * 360);
    turtle.root.position.x -= 0.01;
    turtle.root.rotation.y += degreeToRadian(direction);
  }

  _rotateTowardsFood(turtle) {
    /* Given more time this would've been expanded to turn the turtle towards the food before consuming.
    Currently this is just a pass-through stub. */
    var foodLocation = new THREE.Vector3();
    var turtleLocation = new THREE.Vector3();
    return true;
  }

  _eatFood(turtle) {
    /* Given more time this would've involved an animation, currently the food is simply deleted after collision. */
    swallow(turtle.targetFood);
    turtle.feeding = false;
    turtle.targetFood = null;
  }

  /* Moving forward is normal, but backwards happens at a different speed. */
  _move(turtle, backwards = false) {
    let reversalStrength = 10;
    let speed = 0.01 * t; // animating against t (instead of dt) matches leg rotation
    let direction = turtle.root.rotation.y;
    let dx = speed * Math.cos(direction);
    let dz = speed * Math.sin(-direction);
    if (backwards == true) {
      turtle.root.position.x -= dx * reversalStrength; // Move backwards a variable amount of times as fast.
      turtle.root.position.z -= dz * reversalStrength;
    } else {
      turtle.root.position.x += dx;
      turtle.root.position.z += dz;
    }
  }

  _wag(tailObj) {
    let positionValues = [
      degreeToRadian(0),
      degreeToRadian(-5),
      degreeToRadian(0),
      degreeToRadian(5),
      degreeToRadian(0)
    ];
    let radianChange = interpolator(this.timeKeys, positionValues, t);
    tailObj.tailbone.rotation.z = radianChange;
  }

  _nod(headObj) {
    let positionValues = [
      degreeToRadian(0),
      degreeToRadian(10),
      degreeToRadian(0),
      degreeToRadian(-10),
      degreeToRadian(0)
    ];
    let radianChange = interpolator(this.timeKeys, positionValues, t);
    headObj.neck.rotation.z = radianChange;
  }

  _rotateRightLegs(legs) {
    let positionValues = [
      degreeToRadian(0),
      degreeToRadian(30),
      degreeToRadian(0),
      degreeToRadian(-30),
      degreeToRadian(0)
    ];
    let radianChange = interpolator(this.timeKeys, positionValues, t);
    legs.forEach(legObj => {
      legObj.hip.rotation.y = radianChange;
    });
  }

  _rotateLeftLegs(legs) {
    let positionValues = [
      degreeToRadian(0),
      degreeToRadian(-30),
      degreeToRadian(0),
      degreeToRadian(30),
      degreeToRadian(0)
    ];
    let radianChange = interpolator(this.timeKeys, positionValues, t);
    legs.forEach(legObj => {
      legObj.hip.rotation.y = radianChange;
    });
  }
}

const MANUAL_JOINT_ROTATION_SPEED = 2;

/* Handles the joint rotation for a specific turtle. */
export class JointRotator {
  constructor(turtle) {
    this.gain = degreeToRadian(MANUAL_JOINT_ROTATION_SPEED);
    this.body = {
      hip: [
        turtle.body.frLeg.hip,
        turtle.body.flLeg.hip,
        turtle.body.rrLeg.hip,
        turtle.body.rlLeg.hip
      ],
      knee: [
        turtle.body.frLeg.knee,
        turtle.body.flLeg.knee,
        turtle.body.rrLeg.knee,
        turtle.body.rlLeg.knee
      ],
      ankle: [
        turtle.body.frLeg.ankle,
        turtle.body.flLeg.ankle,
        turtle.body.rrLeg.ankle,
        turtle.body.rlLeg.ankle
      ],
      neck: [turtle.body.top.neck],
      tail: [turtle.body.tail.tailbone]
    };
    this.parameters = {
      hip: { max: degreeToRadian(90), min: degreeToRadian(-90), axis: "z" },
      knee: { max: degreeToRadian(0), min: degreeToRadian(-90), axis: "y" },
      ankle: { max: degreeToRadian(60), min: degreeToRadian(-60), axis: "y" },
      neck: { max: degreeToRadian(90), min: degreeToRadian(-90), axis: "z" },
      tail: { max: degreeToRadian(90), min: degreeToRadian(-90), axis: "z" }
    };
  }

  /* Identify the select joint, pair with rotation parameters, and increment/decrement depending on a flag */
  rotate(jointName, decrement) {
    let joints = this.body[jointName];
    let gain = this.gain;
    let params = this.parameters[jointName];
    if (decrement == true) {
      gain *= -1;
    }

    joints.forEach(joint => {
      // The rotation exceeds the maximum boundary.
      if (joint.rotation[params.axis] > params.max) {
        joint.rotation[params.axis] = params.max;
      }
      // The rotation exceeds the minimum boundary.
      else if (joint.rotation[params.axis] < params.min) {
        joint.rotation[params.axis] = params.min;
      }
      // Otherwise we perform the increment/decrement.
      else {
        joint.rotation[params.axis] += gain;
      }
    });
  }
}
