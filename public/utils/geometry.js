/*
 * [ Overview ]
 * A series of functions related to generating objects for use in the scene.
 * These are simply building blocks to create our turtles.
 */

export function createAxes(length) {
  var geometry = new THREE.Geometry();
  geometry.vertices.push(new THREE.Vector3(0, 0, 0));
  geometry.vertices.push(new THREE.Vector3(length, 0, 0));
  geometry.vertices.push(new THREE.Vector3(0, 0, 0));
  geometry.vertices.push(new THREE.Vector3(0, length, 0));
  geometry.vertices.push(new THREE.Vector3(0, 0, 0));
  geometry.vertices.push(new THREE.Vector3(0, 0, length));
  geometry.colors.push(new THREE.Color(0xff0000));
  geometry.colors.push(new THREE.Color(0xff0000));
  geometry.colors.push(new THREE.Color(0x00ff00));
  geometry.colors.push(new THREE.Color(0x00ff00));
  geometry.colors.push(new THREE.Color(0x0000ff));
  geometry.colors.push(new THREE.Color(0x0000ff));

  var material = new THREE.LineBasicMaterial();
  material.vertexColors = THREE.VertexColors;

  var axes = new THREE.LineSegments(geometry, material);
  axes.name = "axes";

  axes.visible = false; // Start hidden.

  return axes;
}

const DEFAULT_MATERIAL = new THREE.MeshBasicMaterial({
  color: 0xffff00,
  wireframe: false,
  side: THREE.DoubleSide
});

// Returns square bipyramid (octahedron aka. diamond) object.
// There are no assumed dimensions, this is handled by the caller.
export function createSquareBipyramid(
  sizeX,
  sizeY = sizeX * 0.6,
  sizeZ = sizeY,
  material = DEFAULT_MATERIAL
) {
  // Create geometry.
  var geometry = new THREE.Geometry();

  // Create vertices.
  // Side View (Left, Back, Right, Front, Top, Bottom).
  geometry.vertices.push(
    new THREE.Vector3(-sizeX, 0, 0),
    new THREE.Vector3(0, 0, sizeZ),
    new THREE.Vector3(sizeX, 0, 0),
    new THREE.Vector3(0, 0, -sizeZ),
    new THREE.Vector3(0, sizeY, 0),
    new THREE.Vector3(0, -sizeY, 0)
  );

  // Create faces.
  geometry.faces.push(
    new THREE.Face3(0, 5, 1),
    new THREE.Face3(1, 5, 2),
    new THREE.Face3(2, 5, 3),
    new THREE.Face3(3, 5, 0),
    new THREE.Face3(0, 4, 1),
    new THREE.Face3(1, 4, 2),
    new THREE.Face3(2, 4, 3),
    new THREE.Face3(3, 4, 0)
  );

  // The face normals and vertex normals can be calculated automatically if not supplied above.
  geometry.computeFaceNormals();
  geometry.computeVertexNormals();

  // Et voila..!
  var object = new THREE.Mesh(geometry, material);

  return object;
}

// To calculate the corners of the hexagonal component of the dodecahedron.
function _pointy_hex_corner(center, size, i) {
  var angle_deg = 60 * i - 180; // Top Anti-Clockwise
  var angle_rad = (Math.PI / 180) * angle_deg;
  return [
    center["x"] + size * Math.sin(angle_rad),
    center["y"] + size * Math.cos(angle_rad)
  ];
}

// Returns hexagonal bipyramid (dodecahedron) object.
export function createHexagonalBipyramid(
  diameter,
  height,
  material = DEFAULT_MATERIAL
) {
  // Create geometry.
  var geometry = new THREE.Geometry();

  // Create vertices.
  // Top View (Anti-Clockwise from Top).
  for (let i = 0; i < 6; i++) {
    let hex_center = { x: 0, y: 0 };
    let hex_diameter = 1;
    let hex_corner = _pointy_hex_corner(hex_center, hex_diameter, i);

    let x = hex_corner[0];
    let z = hex_corner[1];
    geometry.vertices.push(new THREE.Vector3(x, 0, z));
  }

  // Top & Bottom.
  geometry.vertices.push(new THREE.Vector3(0, height, 0));
  geometry.vertices.push(new THREE.Vector3(0, -height, 0));

  // Create faces.
  geometry.faces.push(
    new THREE.Face3(0, 1, 6),
    new THREE.Face3(1, 2, 6),
    new THREE.Face3(2, 3, 6),
    new THREE.Face3(3, 4, 6),
    new THREE.Face3(4, 5, 6),
    new THREE.Face3(5, 0, 6),
    new THREE.Face3(0, 1, 7),
    new THREE.Face3(1, 2, 7),
    new THREE.Face3(2, 3, 7),
    new THREE.Face3(3, 4, 7),
    new THREE.Face3(4, 5, 7),
    new THREE.Face3(5, 0, 7)
  );

  // The face normals and vertex normals can be calculated automatically if not supplied above.
  geometry.computeFaceNormals();
  geometry.computeVertexNormals();

  // Et voila..!
  var object = new THREE.Mesh(geometry, material);
  object.name = "hexagonalbipyramid";

  return object;
}
